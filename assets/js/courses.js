//gets a string
let adminUser = localStorage.getItem('isAdmin')
let addButton = document.querySelector('#adminButton')
let activeCourse = localStorage.getItem('isActive')
let token = localStorage.getItem('token')

console.log(adminUser)
console.log(token)

let cardFooter 

if (adminUser === "true"){
	
	addButton.innerHTML +=
	`<div class="col-md-2 offset-md-10">
	<a href="../pages/addCourse.html" class="btn btn-primary"> Add Course </a>
	</div>
	`


} else {

	addButton.innerHTML +=
	' '
}


console.log(adminUser==="true" && adminUser !== null)

if (adminUser==="true" && adminUser !== null){
fetch('https://mysterious-ridge-05127.herokuapp.com/api/courses/all',{
		headers: {

		"Authorization": `Bearer ${token}`

		}
	})

	.then(res => res.json())
	.then(data => {

	console.log(data)
	let courseData;



	//to check if there are any active courses
	if(data.length < 1){

		courseData = "No Courses Available"

	}else {

		courseData = data.map(course => {

			console.log(course.isActive)
			// boolean string if user is admin or not
			if(course.isActive === true){

				//button to go to specific course
				//only Go To Course Button will appear if user is not an admin
				cardFooter = `<a href="./course.html?courseId=${course._id}" class="btn btn-primary text-white btn-block" id="hov">Go To Course</a>

					<a href="./deleteCourse.html?courseId=${course._id}" class="btn btn-danger text-white btn-block" id="hov">Archive Course</a>`

			
			} else{

				//buttons go to course or archive if userAdmin == true
				cardFooter = ` <a href="./course.html?courseId=${course._id}" class="btn btn-primary text-white btn-block" id="hov">Go To Course</a>

					<a href="./activateCourse.html?courseId=${course._id}" class="btn btn-success text-white btn-block" id="hov">Activate Course</a>`
			

			}
			return (

				`	
					<div class="col-md-6 my-3">
						<div class="card">
							<div class="card-body">
								<h5 class="card-title">${course.name}</h5>
								<p class="card-text text-left">${course.description}</p>
								<p class="card-text text-right">&#8369 ${course.price}</p>
							</div>
							<div class="card-footer">
								${cardFooter}
							</div>
							
							</div>
						</div>
					</div>

				`

				)
		

		}).join("")

		//join() - joins all of the array elements/items into a string. The argument passed in the method becomes the separator for each item because by default, each item is separated by a comma. 

	}
		console.log(courseData)
	//store the element with the id coursesContainer in a variable.
	let container = document.querySelector("#coursesContainer")

	container.innerHTML = courseData 
})

} else {

fetch('https://mysterious-ridge-05127.herokuapp.com/api/courses/')
.then(res => res.json())
.then(data => {

	console.log(data)
	let courseData;



	//to check if there are any active courses
	if(data.length < 1){

		courseData = "No Courses Available"

	}else {

		courseData = data.map(course => {

			console.log(course)
			// boolean string if user is admin or not
			if(token === null && adminUser === null){

				//button to go to specific course
				//only Go To Course Button will appear if user is not an admin
				cardFooter = `<a href="./course.html?courseId=${course._id}" class="btn btn-primary text-white btn-block">View Course</a>`

			
			} else  

				//buttons go to course or archive if userAdmin == true
				cardFooter = ` <a href="./course.html?courseId=${course._id}" class="btn btn-primary text-white btn-block">Go To Course</a>`
			

			
			return (

				`	
					<div class="col-md-6 my-3">
						<div class="card">
							<div class="card-body">
								<h5 class="card-title">${course.name}</h5>
								<p class="card-text text-left">${course.description}</p>
								<p class="card-text text-right">${course.price}</p>
							</div>
							<div class="card-footer">
								${cardFooter}
							</div>
							
							</div>
						</div>
					</div>

				`

				)

		}).join("")

		//join() - joins all of the array elements/items into a string. The argument passed in the method becomes the separator for each item because by default, each item is separated by a comma. 

	}
		console.log(courseData)
	//store the element with the id coursesContainer in a variable.
	let container = document.querySelector("#coursesContainer")

	container.innerHTML = courseData 

})
	

}