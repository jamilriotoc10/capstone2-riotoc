let navSession = document.querySelector("#navSession")
let userName = document.querySelector('#userProfile') //this is where the name will be placed
let lastName = document.querySelector('#lastName')
let noAdmin = document.querySelector('#noAdmin')
let mobileNo = document.querySelector('#mobileNo')
let adminUser = localStorage.getItem('isAdmin')
let token = localStorage.getItem("token")



console.log(adminUser)

/* name, mobileNo, in profile page of regular user */


if (adminUser === "false"){


fetch('https://mysterious-ridge-05127.herokuapp.com/api/users/details', {

		headers: {

			Authorization: `Bearer ${token}`

		}

	})
	.then(res => res.json())    
	.then(data => {
		
		userName.innerHTML = `${data.firstName} ${data.lastName}`
		mobileNo.innerHTML = `${data.mobileNo}`

	})

/* no items in profile page if user is admin*/
} else {

	noAdmin.innerHTML = ' '
	
} /* END OF PROFILE HEADING*/

/* START OF ENROLLED SUBJECTS IN USER PROFILE*/

if (adminUser === "false"){
fetch('https://mysterious-ridge-05127.herokuapp.com/api/users/details', {

	headers: {

		Authorization: `Bearer ${token}`

	}

})
.then(res => res.json())    
.then(data => {
		
		let enrolledCourses = data.enrollments 
	
		console.log(enrolledCourses)

		let userCourses ; 

	if(enrolledCourses.length < 1){

		userCourses = 'No Course Found.'
	}

	else {

		userCourses = data.enrollments.map(course => {

			console.log(course)
			return (

				`	
					<div class="col-md-6 my-3">
						<div class="card">
							<div class="card-body">
								<h5 class="card-title">${course.courseId}</h5>
								<p class="card-text text-left">${course.enrolledOn}</p>
								<p class="card-text text-right">${course.status}</p>
							</div>
						</div>
					</div>

				`

				)

		}).join("")
	}
	//console.log(userCourses)

	let userEnrolledCourses = document.querySelector('#profileContainer')

	userEnrolledCourses.innerHTML += `${userCourses}`
})
} else {

	userCourses = " "
}














//if adminuser, no provile can be viewed <<<< ---- IMPORTANT
/*if (adminUser === "true"){
	
	navSession.innerHTML +=
	`
	`


} else {

	navSession.innerHTML +=
	`
	<li class="nav-item">
			<a href="./profile.html" class="nav-link"> Profile </a>
	</li>
	`
}*/
