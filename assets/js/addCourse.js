let formSubmit = document.querySelector("#createCourse")
let adminUser = localStorage.getItem("isAdmin")
console.log(adminUser)

if(adminUser === "true" && adminUser !== null){
formSubmit.addEventListener("submit", (e) => {

	e.preventDefault()

	let courseName = document.querySelector("#courseName").value
	let description = document.querySelector("#courseDescription").value
	let price = document.querySelector("#coursePrice").value
	let token = localStorage.getItem("token")
	



	fetch('https://mysterious-ridge-05127.herokuapp.com/api/courses/', {

		method: "POST",
		headers: {
						

			Authorization: `Bearer ${token}`,
			"Content-Type": "application/json"


			},

			body: JSON.stringify({

						name: courseName,
						description: description,
						price: price
					})


			})
			.then(res => res.json())
			.then(data => {
					 
					//console.log(data)

			if(data){

			//alert("Course Add Successful")

			window.location.replace("./courses.html")
					
					
			}else{

			alert("Something went wrong. Course not added")

			}
					
		})


})
}else{

	alert("You are not an admin.")
	window.location.replace("./courses.html")
}