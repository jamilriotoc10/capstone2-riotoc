
let params = new URLSearchParams(window.location.search)
let courseId = params.get('courseId')
let adminUser = localStorage.getItem('isAdmin')
let token = localStorage.getItem("token")

let courseName = document.querySelector("#courseName")
let courseDesc = document.querySelector("#courseDesc")
let coursePrice = document.querySelector("#coursePrice")
let enrollContainer = document.querySelector("#enrollContainer")
let editCourse = document.querySelector("#editCourse")



if (adminUser !== "true"){

fetch(`https://mysterious-ridge-05127.herokuapp.com/api/courses/${courseId}`)
.then(res => res.json())
.then(data => {
	//console.log(data)

	courseName.innerHTML = `${data.name}`
	courseDesc.innerHTML = `${data.description}`
	coursePrice.innerHTML = `${data.price}`

	enrollContainer.innerHTML = `<button id="enrollButton" class="btn btn-block btn-primary">Enroll</button>`

	//add a click event to our button

	document.querySelector("#enrollButton").addEventListener("click", ()=>{

		//no need to add e.preventDefault because the click event doest not have a default behavior that refreshes the page unlike submit.
	
		fetch("https://mysterious-ridge-05127.herokuapp.com/api/users/enroll", {

			method: 'POST',
			headers: {

				"Content-Type": "application/json",
				"Authorization": `Bearer ${token}`

			},
			body: JSON.stringify({

				courseId: courseId

			})


		})
		.then(res => res.json())
		.then(data => {

			 if(data === true){


			window.location.replace("./courses.html")
					
					
			}else{

			alert("You need to login or register to continue.")	
			window.location.replace("./login.html")

			}
		})
	})
	



})

} else {

	if(adminUser === "true"){

	fetch(`https://mysterious-ridge-05127.herokuapp.com/api/courses/${courseId}`)
		.then(res => res.json())
		.then(data => {

		courseName.innerHTML = `${data.name}`
		courseDesc.innerHTML = `${data.description}`
		coursePrice.innerHTML = `${data.price}`

		editCourse.innerHTML = `<button id="editButton" class="btn btn-block btn-primary">Edit Course</button>`

		let enrollees = data.enrollees

	console.log(data.enrollees)

	 	let	userEnrollees;

		if(enrollees.length < 1) {

			userEnrollees = 'No Enrolled Student.'

		}else{
			userEnrollees = data.enrollees.map(user => {
				return (

				`	
					<div class="col-md-12 my-3">
						<div class="card">
							<div class="card-body">
								<h5 class="card-title">${user.userId}</h5>
								<p class="card-text text-center">${user.enrolledOn}</p>
							</div>
						</div>
					</div>

				`

				)


			}).join("")

			console.log(userEnrollees)
			enrollContainer.innerHTML = `${userEnrollees}`
		}
	})

	}
}
	//


	
	

	