let navItems = document.querySelector("#navSession");

let userToken = localStorage.getItem("token");


if(!userToken) {

	navItems.innerHTML += 
	`
		<li class="nav-item ">
			<a href="./login.html" class="nav-link"> Log in </a>
		</li>
		<li class="nav-item">
			<a href="./register.html" class="nav-link"> Register </a>
		</li>
	`
} else {

	navItems.innerHTML += 
	`
	<li class="nav-item">
		<a href="./logout.html" class="nav-link"> Logout </a>
	</li>

	`
}