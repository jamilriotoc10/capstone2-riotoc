let navItems = document.querySelector("#navSession");
let userToken = localStorage.getItem("token");
let userGreeting = document.querySelector('#userGreeting')

if(!userToken) {

	//console.log(navItems.innerHTML)
	navItems.innerHTML += 
	`
		<li class="nav-item ">
			<a href="./pages/login.html" class="nav-link"> Log in </a>
		</li>
		<li class="nav-item">
			<a href="./pages/register.html" class="nav-link"> Register </a>
		</li>
	`
} else {

	navItems.innerHTML += `
	<li class="nav-item">
		<a href="./pages/logout.html" class="nav-link"> Logout </a>
	</li>

	`
}



let isAdmin = localStorage.getItem('isAdmin')
console.log(isAdmin)

if (!userToken){

	userGreeting.innerHTML +=
		`Hello Guest!`
}else {

	if (isAdmin === "true") {

		userGreeting.innerHTML += 
			`Hello, Admin!`
	}else{

		userGreeting.innerHTML +=
			`Hello User!`
		}

	
}