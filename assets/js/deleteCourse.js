
let params = new URLSearchParams(window.location.search)
let courseId = params.get('courseId')
let token = localStorage.getItem("token")
//console.log(courseId)

fetch(`https://mysterious-ridge-05127.herokuapp.com/api/courses/deactivate/${courseId}`, {

	method: "PUT",
	headers: {

		"Authorization": `Bearer ${token}`

	}
})
.then(res => res.json())
.then(data => {

	if(data) {

		alert("Course Archived.")
		window.location.replace("./courses.html")

	} else {

		alert("Something Went Wrong.")
		window.location.replace("./courses.html")
	}

})

